# Ansible Role: Terrascan

This role installs [Terrascan](https://github.com/accurics/terrascan/) binary on any supported host.

## Requirements

github3.py >= 1.0.0a3

It can be installed from pip:

```bash
pip install github3.py
```

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    terrascan_version: latest # tag "v1.13.0" if you want a specific version
    terrascan_arch: x86_64 # x86_64, i386 or arm64
    setup_dir: /tmp
    terrascan_bin_path: /usr/local/bin/terrascan
    terrascan_url: https://github.com/accurics/terrascan/releases/download

This role can install the latest or a specific version. See [available terrascan releases](https://github.com/accurics/terrascan/releases/) and change this variable accordingly.

    terrascan_version: latest # tag "v1.13.0" if you want a specific version

The path of the terrascan repository.

    terrascan_repo_path: https://github.com/accurics/terrascan/releases/download

The location where the terrascan binary will be installed.

    terrascan_bin_path: /usr/local/bin/terrascan

terrascan supports x86_64, arm64 and i386 CPU architectures, just change for the main architecture of your CPU

    terrascan_arch: x86_64 # x86_64, i386 or arm64

Terrascan needs a GitHub token so it can be downloaded. You must use the **github_token** variable in `vars/main.yml` in order to install Terrascan. However, this value can be empty until GitHub reaches the maximum number of anonymous connections.

## Dependencies

None.

## Example Playbook

    - hosts: all
      become: yes
      roles:
        - role: terrascan

## License

MIT / BSD
